#include "OutputFileSystem.h"
#include <time.h>
#include <String>

void OutputFileSystem::makeOFSFile(){

	time_t mt;
	struct tm *t;
	time(&mt);
	t = localtime(&mt);
	char str[100];
	sprintf(str, "%d", t->tm_mon+1);
	strcpy(address, str);
	sprintf(str, "%d", t->tm_mday);
	strcat(address, str);
	sprintf(str, "%d", t->tm_hour);
	strcat(address, str);
	sprintf(str, "%d", t->tm_min);
	strcat(address, str);
	sprintf(str, "%d", t->tm_sec);
	strcat(address, str);
	strcat(address,".txt");

	if ((f = fopen(address, "w+")) == NULL){
		exit(1);
	}
	return;
}

void OutputFileSystem::wrtiteFile(int num,int successflag, int movements, std::list<Cordinate*> L){
	if (f == NULL)exit(0);
	fprintf(f,"Num,%d,Success,%d,movements,%d,",num,successflag,movements);
	for (std::list<Cordinate*>::iterator itr = L.begin(); itr != L.end(); itr++){
		//fprintf(f,"%d-%d,",(*itr)->x,(*itr)->y);
	}
	fprintf(f,"\n");
}

/*別途全体の結果とか書く関数を作る。そこでfcloseして差し上げろ*/

void OutputFileSystem::terminateOFS(int SceneNum, int successNum, int firstSuccessNum, int MinNum){//terminate関数、処理を終わらせる。←引数部にリザルト数値追加なー

	fprintf(f,"Success: %d / %d ,,FirstSccess:,%d,,MinSccess:,%d",successNum,SceneNum,firstSuccessNum,MinNum);

	fclose(f);
}